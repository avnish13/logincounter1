var jwt = require('jsonwebtoken');
function verifyToken(req, res, next) {
    console.log(req.headers);
  
  var token = req.headers['authorization'];
  console.log(token);
  if (!token)
    return res.status(403).send({ auth: false, message: 'Login Required ' });
  jwt.verify(token,'secret', function(err, decoded) {
    if (err)
    return res.status(500).send({ auth: false, message: 'Failed to authenticate.' }); 
    req.email = decoded.email;
    next();
  });
}
module.exports = verifyToken;