const express = require('express');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const _ = require('lodash');
const bcrypt = require('bcryptjs')
const VerifyToken = require('./verifyToken');
const jwt = require('jsonwebtoken');
const User = require('../models/userModel');
const router = express.Router();


router.post('/signup',(req,res)=>{
  const body = _.pick(req.body,['email','name','gender','password']);
  User.findOne({'email':body.email},(err,user)=>{
    console.log(user);
    if(err){
      res.send(err);
    }else if(user){
      console.log("user Exist ");
      res.json({
        'success':false,
        'msg':'User with email exist'
      })
    }else{
      var newUser = new User(body);
      bcrypt.hash(newUser.password,10).then((hash)=>{
        newUser.password=hash;
        newUser.save().then((user)=>{
          var token = jwt.sign({email:user.email},'secret',{expiresIn:86400});
          res.status(200).json({
            'success':true,
            'msg':'Registration sucessful, Now you can login',
            'token':token
          });
        }),(er)=>{
          res.json({
            'success':false,
            'msg':er
          })
        }
      }).catch((er)=>{
        res.json({
          'success':false,
          'msg':er
        })
      });


    }
  });



});

router.post('/signin',(req,res)=>{
  const body = _.pick(req.body,['email','password']);
  console.log(`***${body.password} *** ${body.email}`);

  
  
  User.findByEmail(body.email,body.password,(err,user)=>{
      if(err){
          console.log(err)
          res.json({
              'success':false,
              'msg':"error"
          })
      }else if(!user){
          res.json({
              'success':false,
              'msg':'User not found'
          })
      }
      else if(user){

          user.count=user.count+1;
          user.save().then((user)=>{
            let token = jwt.sign({ email: user.email },'secret', {
              expiresIn: 86400 // expires in 24 hours
            });
          res.json({
              'success':true,
              'msg':'Successfully logged In',
              'token': token,
              'email': user.email 
          })
              
          }),(er)=>{
            res.status(500).json({
                'success':false,
                'msg':"er"
            });
          }
          
      }

  })
});

router.get('/profile', VerifyToken, function(req, res) {
  console.log("/profile in");
    
  User.findOne({email:req.email}, function (err, user) {
      if (err) return res.status(500).send(err);
      if (!user) return res.status(404).send("No user found.");

      res.status(200).json({
        'success':true,
        'msg':user
    });
      
      
    });
   
});


module.exports = router;
