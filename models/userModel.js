var mongoose = require('mongoose');
var Schema = mongoose.Schema;
const bcrypt = require('bcryptjs');
const _ = require('lodash');

var UserSchema = new Schema({
   name:{
       type:String
   },
   email:{
       type:String,
       required:true,
       unique:true
   },
   password:{
    type:String,
    minlength:8,
    required:true
    },
    gender:{
        type:String,
        default:""
    },
    count:{
        type:Number,
        default:0
    }
});
UserSchema.statics.findByEmail=function(email,password,done){
    console.log(" userModel");
    var User = this;
    User.findOne({'email':email},(err,user)=>{
        if(err){
            return done(err)
        }else if(!user){
            return done(null,null);
        }else{
            bcrypt.compare(password, user.password).then((res)=>{
                if(!res){
                    return done('WP',user);
                }else if(res){
                    return done(null,user);
                }
            }).catch((err)=>{
                return done(err);
            })
        }
    })
}

module.exports = mongoose.model("User",UserSchema)